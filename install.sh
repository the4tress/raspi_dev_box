#!/bin/bash

##
# This is a build script for a Raspberry Pi.
#
# It is what I have found to be the best combination of tools when working
# with most home automation projects.
#
# This is a list of tools that are installed after running this script:
#  - nvm
#  - NodeJS v8.11.3
#  - pyenv
#  - Python v3.7.0
#  - c9/core (http://ip)
#  - Ino
#  - Ungit (http://ip/ungit)
#  - Node-RED (http://ip/node-red and http://ip/api)
#  - Webmin (https://ip:10000)
#  - Nginx withh all web services running behing a reverse proxy
#
##

# Installation

# echo "Starting installation. Installing basics..."
# echo ""
# sleep 2

# cd ~
# sudo apt-get update
# sudo apt-get upgrade -y
# sudo apt-get install -y htop iftop tmux ncdu vim git nginx
# sudo systemctl enable nginx
git clone https://gitlab.com/the4tress/raspi_dev_box.git
sudo sed -i -- "s/\$HOSTNAME/$HOSTNAME/g" raspi_dev_box/nginx.conf
sudo cp raspi_dev_box/nginx.conf /etc/nginx/sites-available/dev
sudo rm /etc/nginx/sites-enabled/default
sudo ln -s /etc/nginx/sites-avialable/dev /etc/nginx/sites-enabled/dev

# echo "Installing build files"
# echo ""
# sleep 2

# sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev openssl bzip2 perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python

# # Install webmin
# echo "Installing Webmin. It will be available on port 10000 https.."
# echo ""
# sleep 2

# wget http://prdownloads.sourceforge.net/webadmin/webmin_1.881_all.deb
# sudo dpkg --install webmin_1.881_all.deb
# rm /webmin_1.881_all.deb

# Install nvm
echo "Installing nvm..."
echo ""
sleep 2

git clone https://github.com/creationix/nvm.git ~/.nvm
echo "source ~/.nvm/nvm.sh" >> ~/.bashrc


# Install pyenv
echo "Installing pyenv..."
echo ""
sleep 2

curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
echo 'export PATH="~/.pyenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc

session="rpi"
script="~/raspi_dev_box/tmux.sh"

systime="$( uptime | cut -d 'u' -f 1 | tr -d ' ' )"
sysuptime="Uptime: $( uptime | cut -d ' ' -f 4 | tr -d ',' )"
sysusers="Users: $( uptime | cut -d ',' -f 2 | cut -d 'u' -f 1 | tr -d ' ' )"
syscpu="CPU: $( uptime | cut -d ',' -f 3- | cut -d ' ' -f 5- | tr -d ' ' )"
sysinfo="#[fg=yellow]#[$systime][$sysuptime][$sysusers][$syscpu]"

tmux start-server # Start the tmux server
tmux new-session -d -s $session -n system

tmux set -g -t $session status-bg black                                # Set the background color to black
tmux set -g -t $session status-fg white                                # Set the foreground color to white
tmux set-window-option -g -t $session window-status-current-bg red     # Set the currently selected window background color to red
tmux set -g -t $session status-left-length 12                           # Set the max length of the info text on the left to 12
tmux set -g -t $session status-left '#[fg=green]Raspi Dev Box Install:'           # Set the info text on the left
tmux set -g -t $session status-right '$sysinfo'                         # Set the info text on the right to the system info
tmux set -g -t $session status-right-length 50                          # Set the max length of the info text on the right to 50
tmux set -g -t $session status-interval 1                               # Set the update interval for the status bar to 5 seconds

tmux select-window -t $session:0             # Select the first window
tmux send-keys -t $session:0 "htop" C-m      # Start htop
tmux split-window -h                         # Split the window horizontally
# tmux send-keys -t $session:0 "iftop" C-m     # Start iftop
# tmux send-keys -t $session:0 stSD Enter      # Disable local hostname, make single list, enable source and destination ports
# tmux split-window                            # Split the window vertically
# tmux send-keys -t $session:0 "chmod +x $script" C-m
# tmux send-keys -t $session:0 "$script" C-m   # Install everything else
# Install NodeJS
tmux rename-window -t $session:0 "Installing NodeJS v8.11.3"
tmux send-keys -t $session:0 "nvm install v8.11.3" C-m

# Install c9
tmux rename-window -t $session:0 "Installing c9. This will be the default UI on port 80 http"
tmux send-keys -t $session:0 "git clone https://github.com/c9/core.git .c9sdk" C-m
tmux send-keys -t $session:0 "cd .c9sdk" C-m
tmux send-keys -t $session:0 "scripts/install-sdk.sh" C-m
tmux send-keys -t $session:0 "cd ~" C-m


# Install ino
# tmux rename-window -t $session:0 "Installing ino..."
# tmux send-keys -t $session:0 "pip install ino" C-m


# Install ungit
tmux rename-window -t $session:0 "Installing ungit..."
tmux send-keys -t $session:0 "npm install -g ungit" C-m

# Install Node-RED
tmux rename-window -t $session:0 "Installing Node-RED..."
tmux send-keys -t $session:0 "npm install -g --unsafe-perm node-red" C-m


# Install pm2
tmux rename-window -t $session:0 "Installing pm2. This will auto start apps..."
tmux send-keys -t $session:0 "npm install -g pm2" C-m


# Configure pm2
tmux rename-window -t $session:0 "Making everything auto-start"
tmux send-keys -t $session:0 "pm2 startup" C-m
tmux send-keys -t $session:0 "pm2 start ~/.c9sdk/server.js -- -w /home/pi" C-m
tmux send-keys -t $session:0 "pm2 start node-red" C-m
tmux send-keys -t $session:0 "pm2 start ungit" C-m
tmux send-keys -t $session:0 "pm2 save" C-m


tmux rename-window -t $session:0 "Finished installations. Please reboot."
tmux attach-session -t $session # Attach the session

