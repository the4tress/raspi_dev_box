This will install everything required for turning a Raspberry Pi into a development box for IoT devices.

Usage:

From a fresh Raspbian image, configure it for your country, timezone, wifi, etc.

    sudo raspi-config

After you have you're locale and everything configured (try typing an `@` in the terminal, make sure you really get an `@` and not a `"`), run the following command:

    https://gitlab.com/the4tress/raspi_dev_box/raw/master/install.sh | bash

This will install and configure the following tools and services:

- nvm
- NodeJS v8.11.3
- pyenv
- Python v3.7.0
- c9/core (http://ip)
- Ino
- Ungit (http://ip/ungit)
- Node-RED (http://ip/node-red and http://ip/api)
- Webmin (https://ip:10000)
- Nginx withh all web services running behing a reverse proxy