# Install NodeJS
echo ""
echo "###"
echo "#"
echo "# Installing NodeJS v8.11.3 "
echo "#"
echo "###"
echo ""
sleep 2
nvm install v8.11.3

# Install c9
echo ""
echo "###"
echo "#"
echo "# Installing c9. This will be the default UI on port 80 http"
echo "#"
echo "###"
echo ""
sleep 2

git clone https://github.com/c9/core.git .c9sdk
cd .c9sdk
scripts/install-sdk.sh
cd ~


# Install ino
echo ""
echo "###"
echo "#"
echo "# Installing ino..."
echo "#"
echo "###"
echo ""
sleep 2

pip install ino


# Install ungit
echo ""
echo "###"
echo "#"
echo "# Installing ungit..."
echo "#"
echo "###"
echo ""
sleep 2

npm install -g ungit

# Install Node-RED
echo ""
echo "###"
echo "#"
echo "# Installing Node-RED..."
echo "#"
echo "###"
echo ""
sleep 2

npm install -g --unsafe-perm node-red


# Install pm2
echo ""
echo "###"
echo "#"
echo "# Installing pm2. This will auto start apps..."
echo "#"
echo "###"
echo ""
sleep 2

npm install -g pm2


# Install pm2
echo ""
echo "###"
echo "#"
echo "# Making everything auto-start"
echo "#"
echo "###"
echo ""
sleep 2

pm2 startup
pm2 start ~/.c9sdk/server.js -- -w /home/pi
pm2 start node-red
pm2 start ungit
pm2 save


echo ""
echo "###"
echo "#"
echo "# Finished installations. Rebooting..."
echo "#"
echo "###"
echo ""
sleep 2

sudo reboot